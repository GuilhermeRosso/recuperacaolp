Nome:Guilherme Rosso Cardoso 
Turma:352

1) As estruturas de programa��o das linguagens permitem criar l�gicas para o fluxo dos dados de nossos softwares. Assinale a alternativa que contem uma estrutura de programa��o usada pela linguagem Object-Pascal que permite iterar indefinidamente sobre um mesmo conjunto de intru��es:
	R: E)While 
	Justificativa:Pois ele � infinito se n�o colocar uma condi��o j� o for tem que ter inicio e fim.

2)As estruturas de dados presentes nas linguagens de programa��o s�o necess�rias para que a aplica��o possa entender e armazenar corretamente os dados que pretendemos utilizar, Assinale a alternativa que cont�m, respectivamente, uma estrutura capaz de armazenar apenas n�meros inteiros positivos e negativos e uma estrutura que armazena apenas dois valores:
	R: B)INTEGER e BOOLEAN;
	Justificativa:Pois � a unica que apresenta uma capaz de armazenar apenas dois valores que � BOOLEAN e esta colocado em segundo como deve ser.

(3) V�rias linguagens de programa��o permitem a cria��o de enumeradores. Este tipo de estrutura de dados permite criar uma vari�vel que armazena um valor pr�-definido pelo pr�prio programador. Utilizando como refer�ncia a linguagem Object-Pascal, assinale a alternativa que representa a cria��o de um enumerador para os dias de semana: 
          R: d) type diaSemana = (seg, ter, qua, qui, sex);
	Justificativa: pois a sintaxe esta correta e se usa type para criar enumerador.

(4) Existe uma estrutura definida pelo Object-Pascal capaz de agrupar itens de dados de diferentes tipos (ao contr�rio do array, que armazena v�rios itens de mesmo tipo). Assinale a alternativa que representa o nome desta estrutura: 
	R: E)Record
	JUstificativa:Pois � a unica que pode agrupar itens de dados de diferentes tipos.

(5) O Git � um sistema de controle de vers�es desenvolvido por Linus Torvalds e Junio Hamano que facilita o processo de desenvolvimento de software ao ser usado para registrar o hist�rico de oc.4!��es dos arquivos-fonte. Assinale a alternativa que n�o representa uma fun��o do Git: 
	R:b) Impedir o acesso n�o autorizado aos arquivos fonte por meio da autentica��o de usu�rio
	Justificativa: Ela esta errada pois o git n�o tem nada haver com acesso nao autorizado.

(6) Para organizar a �rea de desenvolvimento, o Git implementa diversas �reas com diferentes caracter�sticas dentro de um projeto. Dessa forma, o Git minimiza altera��es desastrosas que podem comprometer a integridade do c�digo. Assinale a alternativa que representa o nome da �rea do Git onde ficam armazenados os arquivos que est�o prontos para serem preservados permanentemente no reposit�rio local (por�m ainda n�o foram): 
	R: A) Stage/Index
	Justificativa:Antes de efetivar as altera��es realizadas, existe ainda uma �rea intermedi�ria chamada de STAGE AREA (ou INDEX), respons�vel por organizar todos os arquivos antes de serem transformados em vers�o (commit) e armazenados no reposit�rio local.

(7) Ao realizar altera��es no c�digo, o Git n�o preserva automaticamente as altera��es efetuadas em seu reposit�rio. Antes de mais nada, � necess�rio indicar ao versionador quais s�o as modifica��es que pretendemos preservar em uma nova vers�o. Assinale a alternativa que preserva APENAS as altera��es realizadas no arquivo stark.php (levando em considera��o que o arquivo do mesmo projeto vingadores.php tamb�m possui altera��es): 
	R:b)git add stark.php
	Justificativa: pois o git add adiciona e para adicionar apenas um em especifico tem que colocar o nome do arquivo e sua extensao.

(8) Quando h� modifica��es listadas no Index do reposit�rio Git, podemos preservar permanentemente estas altera��es em um processo chamado de commit. Assinale a alternativa que melhor descreve o comportamento do comando git commit: 
	R: e.) Cria-se um identificador �nico para o commit e 
as modifica��es s�o preservadas no reposit�rio local 
	Justificativa:pois � realmente criado um identificador proprio e que possue letras e numeros e s�o salvo no repositorio local.

(9) O reposit�rio remoto � uma defini��o do Git para uma c�pia remota do reposit�rio local de um determinado projeto. No entanto, estes dois tipos de reposit�rio podem conter diferen�as entre si, que necessitam da atualiza��o do programador. Assinale a alternativa que representa, respectivamente, o comando utilizado para atualizar o reposit�rio local (com o conte�do remoto) e o comando utilizado para atualizar o reposit�rio remoto (com o conte�do local) 
	R:c)GIT PULL e GIT PUSH
	Justificativa:pois o primeiro e pra atualizar o local com o remoto e o segundo para atualizar o remoto com os dados do local,

(10) Os gerenciadores de reposit�rio baseados em Git (como o GitLab) s�o respons�veis por permitir que desenvolvedores armazenem remotamente seus reposit�rios e oferecem ferramentas para a constru��o do software de maneira colaborativa. Assinale a alternativa que apresenta o comando utilizado para realizar a c�pia de um reposit�rio para a sua m�quina, levando em considera��o: 
Servidor: gitlab.com 
Usu�rio: rvenson 
Reposit�rio: prova01 
	R:a) git clone https://gitlab.com/rvenson/prova01 
	Justificativa:pois o clone serva para pegar os arquivos de um repositorio e a url est� correta.

